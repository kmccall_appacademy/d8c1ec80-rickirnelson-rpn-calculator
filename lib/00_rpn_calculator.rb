class RPNCalculator
  def initialize
    @arr = []
  end

  def push(n)
    @arr << n
  end

  def plus
    if @arr.length < 2
      raise "calculator is empty"
    end
    @arr << [@arr.pop, @arr.pop].reverse.reduce(:+)
  end

  def minus
    if @arr.length < 2
      raise "calculator is empty"
    end
    @arr << [@arr.pop, @arr.pop].reverse.reduce(:-)
  end

  def times
    if @arr.length < 2
      raise "calculator is empty"
    end
    @arr << [@arr.pop, @arr.pop].reverse.reduce(:*)
  end

  def divide
    if @arr.length < 2
      raise "calculator is empty"
    end
    @arr << [@arr.pop.to_f, @arr.pop.to_f].reverse.reduce(:/)
  end

  def value
    @arr[-1]
  end

  def tokens(numstr)
     numstr.split(' ').map {|s| s =~ /[\/*+-]/ ? s.to_sym : s.to_i }
  end

  def evaluate(numstr)
    chunk = numstr.split(' ').map {|s| s =~ /[\/*+-]/ ? s : s.to_i }.slice_when {|i,j| j.class != i.class}.to_a
    chunk = chunk.each_slice(2).map(&:flatten)

    def eq(chunkarr)
      numbers = chunkarr.join("").scan(/["0-9"]/).map(&:to_i).map(&:to_f).map(&:to_s)
      operators = chunkarr.join('').scan(/[^"0-9"a-zA-z" "]/)
      numbers.reverse.each_slice(2).to_a.map {|a| a.reverse}.flatten.zip(operators).flatten.compact.join(' ')
    end

    res = []
    chunk.each do |chunkarr|
      res << eq(chunkarr)
    end

    extra_op = []
    res.map! do |el|
       el[-1] =~ /[\/+*-]/ ? extra_op << el[-1] && el = el[0...-1] : el
       eval el
    end

    res_extra = eval res.zip(extra_op).flatten.compact.map(&:to_s).join(' ')
    extra_op.empty? ? res[0] : res_extra
  end
end
